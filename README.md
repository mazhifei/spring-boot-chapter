## 源码分支简介
`2.x`分支对应`恒宇少年`编写的**SpringBoot2.x**系列文章示例的源码，请根据阅读指南查看文章以及对应的源码目录。

## 专题愿景

旨在打造**全网免费**`精而全`的`SpringBoot`系列技术文章学习专题，从`简单了解`到`核心技术`再到`源码分析`都涵盖在其中，作者在在简书开通的`SpringBoot核心技术`专题，原创的文章总阅读量已达到数百万，这跟大家的关注与支持息息相关！！！


> 本专题采用`SpringBoot2.x`版本进行编写，陆续更新，请多多关注。

## 请给我支持

您的支持是给我创作的最大动力，我会将`编写原创技术类文章`并在第一时间分享给大家。

- 请关注作者的公众号`“程序员恒宇少年”`，二维码在页面底部，关注后回复"资料"可获取专属的电子小册
- 请将该页面分享给更多需要它的技术学习爱好者
- 请给文章对应的代码案例仓库点个`Star`，`Follow`我更可以第一时间了解更新动向
  - [Gitee](https://gitee.com/hengboy/spring-boot-chapter)

## 福利来袭

![](http://blog.yuqiyu.com/images/%E7%AD%BE%E5%88%B0%E7%A6%8F%E5%88%A9.png)

> 自律改变人生 - [签到赠书计划开启~](http://blog.yuqiyu.com/welfare/)

## 作者推荐

如果文字类教程对您吸引度不高或者想要综合学习`Spring全家桶`，推荐给您几个视频教程，价格良心，内容精辟。

- [“玩转Spring全家桶”](https://time.geekbang.org/course/intro/100023501?code=Bb0kYvkF8opyRcznUfl1msqTIMJ1AY-kgBEfuR9N4nw%3D)  `20000+ 人已学习`
- [更多视频教程](http://blog.yuqiyu.com/geektime)

如果你已经对`SpringBoot`有一定的了解，[这个框架](http://blog.yuqiyu.com/apiboot-all-articles.html)应该是你目前所需要的，可以助力让你成为服务架构师。


## 阅读指南



### 基础篇

| 源码目录                                          | 对应文章                                                     |
| ------------------------------------------------- | ------------------------------------------------------------ |
| developing-first-application                      | [SpringBoot2.x基础篇：开发你的第一个SpringBoot应用程序](https://blog.yuqiyu.com/spring-boot-basic-developing-first-application.html) |
| spring-boot-basic-accessing-application-arguments | [SpringBoot2.x基础篇：应用程序在启动时访问启动项参数](https://blog.yuqiyu.com/spring-boot-basic-accessing-application-arguments.html) |
| spring-boot-basic-externalized-configuration      | [SpringBoot2.x基础篇：灵活的使用外部化配置信息](https://blog.yuqiyu.com/spring-boot-basic-externalized-configuration.html) |
| spring-boot-basic-configuring-random-values       | [SpringBoot2.x基础篇：探索配置文件中随机数的实现方式](https://blog.yuqiyu.com/spring-boot-basic-configuring-random-values.html) |



### 新特性

| 源码目录                                       | 对应文章                                                     |
| ---------------------------------------------- | ------------------------------------------------------------ |
| Chapter47                                      | [Quartz在SpringBoot2.x内的自动化配置](http://blog.yuqiyu.com/quartz-springboot2-starter.html) |
| Chapter48                                      | [消息队列RabbitMQ设置信任package](http://blog.yuqiyu.com/rabbitmq-trust-package.html) |
| Chapter49                                      | [SpringBoot2.x内配置WebMvc](http://blog.yuqiyu.com/springboot-mvc-configurer.html) |
| springboot2-2-configuration-binding-bit-pitted | [SpringBoot2.2版本配置绑定是不是有点坑了？](http://blog.yuqiyu.com/springboot2.2-configuration-binding-bit-pitted.html) |
| springboot-constructor-binding-properties      | [SpringBoot使用@ConstructorBinding注解进行配置属性绑定](http://blog.yuqiyu.com/springboot-constructor-binding-properties.html) |



### 核心技术

| 源码目录                              | 对应文章                                                     |
| ------------------------------------- | ------------------------------------------------------------ |
| spring-boot-failure-analyzer          | [SpringBoot详细打印启动时异常堆栈信息](http://blog.yuqiyu.com/springboot-failure-analyzer.html) |
| -                                     | [SpringBoot激活profiles你知道几种方式？](http://blog.yuqiyu.com/several_ways_to_activate_springboot_profiles.html) |
| springboot-integration-using-flyway   | [SpringBoot整合Flyway完成数据库持久化迭代更新](http://blog.yuqiyu.com/springboot-integration-using-flyway.html) |
| use-nginx-loadbalance-upgrade-service | [使用nginx的负载均衡机制实现用户无感更新服务](https://blog.yuqiyu.com/use-nginx-loadbalance-upgrade-service.html) |



### 数据

| 源码目录  | 对应文章                                                     |
| --------- | ------------------------------------------------------------ |
| Chapter50 | [SpringBoot2.x使用Redis缓存数据](http://blog.yuqiyu.com/redis-springboot2-starter.html) |
| Chapter51 | [SpringBoot2.x使用MongoDB存储数据](http://blog.yuqiyu.com/mongodb-springboot2-starter.html) |
| Chapter52 | [SpringBoot2.x使用MongoDB的Rest端点访问数据](http://blog.yuqiyu.com/mongodb-springboot2-rest.html) |




## 作者公众号

  <img src="http://blog.yuqiyu.com/images/mp.jpg" width="150"/>