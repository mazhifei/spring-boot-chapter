package org.minbox.framework.redis.notice.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * Redis内的Key过期后调用该监听
 *
 * @author 恒宇少年
 */
@Component
public class MessageListener extends KeyExpirationEventMessageListener {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(MessageListener.class);

    public MessageListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String key = message.toString();
        logger.info("Redis Key：{}", key);
    }
}
