package org.minbox.chapter.springboot2.configuration.binding;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static org.minbox.chapter.springboot2.configuration.binding.ConfigProperties.CONFIG_PREFIX;


/**
 * 示例：配置参数
 *
 * @author 恒宇少年
 */
@ConfigurationProperties(prefix = CONFIG_PREFIX)
@Data
public class ConfigProperties {
    public static final String CONFIG_PREFIX = "minbox.config";
    /**
     * IP地址
     */
    private String ipAddress;
    /**
     * 端口号
     */
    private String port;
}
